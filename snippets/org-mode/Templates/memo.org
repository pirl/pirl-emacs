#+contributor: Marvin Doyley <m.doyley@rochester.edu>
#+key: memo.org
#+name: Memo  Template
#+group: Template 
# --
#+latex_class: memo
#+latex_header:\usepackage{graphicx}
#+LATEX_HEADER: \newenvironment{response}{\color{red}}{\ignorespacesafterend}
#+latex_header: \memoto{$1 }
#+latex_header: \memofrom{Marvin M. Doyley}
#+latex_header:  \memosubject{$2}
#+latex_header: \memodate{\today}
#+latex_header: \usepackage[linktocpage, pdfstartview=FitH, colorlinks, linkcolor=blue, anchorcolor=blue, citecolor=blue, filecolor=blue, menucolor=blue, urlcolor=blue]{hyperref}
#+latex_header: \logo{\includegraphics[width=0.35\textwidth]{Hajim.pdf}}
#+options: toc:nil
#+options: author:nil
#+latex:\maketitle 


Dear Editor:

Thank you for the opportunity to revise our manuscript. We are pleased to provide a point-by-point response to the reviewer comments below.



Sincerely,\\\
#+latex:\includegraphics[width=0.2\textwidth]{Doyley-signature.pdf}\\\
Marvin M. Doyley, Ph.D.\\\
Associate Professor\\\
University of Rochester\\\
Hajim School of Engineering and Applied Sciences\\\
Department of Electrical & Computer Engineering\\\
Hopeman Building 333\\\
Rochester, NY 14627\\\
Telephone: (585) 275-3774, Fax: (585) 273-4919\\\

* build :noexport:
  :PROPERTIES:
  :CUSTOM_ID: build
  :END:
<<build>> elisp:org-latex-export-to-pdf

