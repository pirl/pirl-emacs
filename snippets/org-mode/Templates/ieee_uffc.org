#contributor : Marvin Doyley <doyley@ece.rochester.edu>
#key: ieee_uffc.org
#name : IEEE UFFC Template
#group: Templates
# --
#+latex_class: uffc
#+LATEX_CLASS_OPTIONS: [12pt,letterpaper,onecolumn]
#+OPTIONS:   H:4 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:nil pri:nil tags:not-in-toc
#+OPTIONS: author:nil date:nil ^:{} toc:nil
#+export_exclude_tags: noexport
#+latex_header: \usepackage[utf8]{inputenc}
#+latex_header: \usepackage{fixltx2e}
#+latex_header: \usepackage{url}
#+latex_header: \usepackage{subfigure,calc}
#+latex_header: \usepackage{color}
#+latex_header: \usepackage{url}
#+latex_header: \usepackage{amsmath}
#+latex_header: \usepackage{textcomp}
#+latex_header: \usepackage{wasysym}
#+latex_header: \usepackage{latexsym}
#+latex_header: \usepackage[T1]{fontenc}
#+latex_header: \usepackage{amssymb}
#+latex_header: \usepackage{amsfonts}
#+latex_header: \usepackage{multirow}
#+latex_header: \usepackage{mathtools}
#+latex_header: \usepackage[margin=1.0in]{geometry}
#+latex_header: \linespread{2}
#+latex_header: \usepackage[linktocpage, pdfstartview=FitH, colorlinks, linkcolor=blue, anchorcolor=blue, citecolor=blue, filecolor=blue, menucolor=blue, urlcolor=blue]{hyperref}

#+BEGIN_EXPORT latex
\title{<replace: title of paper>}

\author{<replace: Author1>$^{1}$,
       <replace: Author2>$^{2}$, 
       <replace: Author3>$^{3}$, 
       and~Marvin M. Doyley$^{4}$,~\IEEEmembership{Member,IEEE,}
\thanks{$^1$<replace:insert address and email>}
\thanks{$^2$Department of Imaging Sciences, School of Medicine and Dentistry, University of Rochester Medical Center NY 14642 email: <replace: insert email>}% <-this % stops a space
\thanks{$^3$<replace:insert address and email>}
\thanks{$^{4}$Hajim School of Engineering and Applied Sciences,\\University of Rochester,Rochester NY 14627 \\(phone: 585-275-3774; fax: 585-273-4919); email:m.doyley@rochester.edu}}

#+END_EXPORT

\maketitle 

\begin{abstract}
Write abstract, this should be a summary of your article.
\end{abstract}
\keywords{<replace: comma separated keywords>}
* Introduction
* Materials and Methods
* Results 
* Discussion
* Conclusions
#+latex:\section{Acknowledgements}
 This work was supported by the National Heart and Lungs Research grant <replace: insert grant
 number(s)>. We would also like to thank <replace: person (s) name> for <replace: how did they help>.
* References
bibliographystyle:IEEEtran
bibliography:<replace: with filename>
* Figure caption
* Miscellaneous                                                    :noexport:
elisp:ox-manuscript-build-submission-manuscript-and-open
