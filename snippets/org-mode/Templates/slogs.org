#contributor: Marvin Doyley <m.doyley@rochester.edu>
#name: Template for making success logs 
#key: slogs.org
#group: Templates
# --
- What did I do well in the last 24 hours?
  1. 
  2. 
  3. 
- What is one thing I plan to improve in the next 24 hours?
  1. 
- What is one thing I can do differently to help make the above mentioned improvements
  1.
- One a 10 point scale how did I do today with RSF
  1.
