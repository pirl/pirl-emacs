#contributor : Marvin Doyley <doyley@ece.rochester.edu>
#key: Med_phys.org
#name : Medical Physics Template
#group: Templates
# --
#+latex_class: medphys
#+LATEX_CLASS_OPTIONS: [number, sort&compress, review, 12pt]
#+OPTIONS:   H:4 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:nil pri:nil tags:not-in-toc
#+OPTIONS: author:nil
#+export_exclude_tags: noexport
#+latex_header: \usepackage[utf8]{inputenc}
#+latex_header: \usepackage{fixltx2e}
#+latex_header: \usepackage{url}
#+latex_header: \usepackage{graphicx}
#+latex_header: \usepackage{float}
#+latex_header: \usepackage{color}
#+latex_header: \usepackage{url}
#+latex_header: \usepackage{cleveref}
#+latex_header: \usepackage{amsmath}
#+latex_header: \usepackage{textcomp}
#+latex_header: \usepackage{wasysym}
#+latex_header: \usepackage{latexsym}
#+latex_header: \usepackage[T1]{fontenc}
#+latex_header: \usepackage{amssymb}
#+latex_header: \usepackage{amsfonts}
#+latex_header: \usepackage{color}
#+latex_header: \usepackage{lineno}
#+latex_header: \usepackage{multirow}
#+latex_header: \usepackage{mathtools}
#+latex_header: \usepackage[margin=1.0in]{geometry}
#+latex_header: \linespread{2}
#+latex_header: \usepackage[linktocpage, pdfstartview=FitH, colorlinks, linkcolor=blue, anchorcolor=blue, citecolor=blue, filecolor=blue, menucolor=blue, urlcolor=blue]{hyperref}
#+latex_header:\linenumbers
#+latex_header: \modulolinenumbers[5]

\title{<replace: title of paper>}
\author{<replace:with author name>}
\affiliation{Hajim school of Engineering and Applied Sciences, University of Rochester, Rochester, New York, 14627, United States}
\author{<replace: copy and past for other authors>}
\affiliation{Department of Imaging Sciences, School of Medicine and Dentistry, University of Rochester Medical Center NY 14642}
\author{Marvin M. Doyley}
\affiliation{Hajim school of Engineering and Applied Sciences, University of Rochester, Rochester, New York, 14627, United States}

\begin{abstract}
Write abstract, this should be a summary of your article.
- Purpose:
- Methods:
- Results:
- Conclusion
\end{abstract}
\keywords{<replace: comma separated keywords>}
#+latex: \maketitle
#+latex: \modulolinenumbers[5]% Line numbers with a gap of 5 lines
* Introduction
* Materials and Methods
* Results 
* Discussion
* Conclusions
#+latex:\section{Acknowledgements}
 This work was supported by the National Heart and Lungs Research grant <replace: insert grant
 number(s)>. We would also like to thank <replace: person (s) name> for <replace: how did they help>.
* References
bibliographystyle:medphys
bibliography:<replace: with filename>
* Figure caption
* Miscellaneous                                                    :noexport:
elisp:ox-manuscript-build-submission-manuscript-and-open
