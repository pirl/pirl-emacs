(set-default-font "-apple-monaco-medium-r-normal--18-0-72-72-m-0-iso10646-1")   ;; My favorite emacs font
(setq org-files "~/Org-files/")                     ;; Directory where I strore my org files
(setq smex-save-file "~/Emacs/smex.save")           ;; Keep an history of the files that I have loaded
(setq brain "Reference/Brain.org")                  ;; File containing all the notes, web clipping, etc. that I want to remember
(setq mytodo "General/Master-todo.org")             ;; file containing todos
(setq logfile "General/Success_log.org")             ;; file containing log files

(setq abbrev-file-name    "~/Emacs/abbrev_defs")    ;; Abbreviation files
(setq ispell-program-name "/usr/local/bin/ispell"   ;; Spell checker
      ispell-personal-dictionary "~/Emacs/dict")    ;; My costum dictionary 

(setq helm-bibtex-bibliography "~/Dropbox/Filing_Cabinet/B/refs/ref.bib")  ;; bibtex file
(setq reftex-default-bibliography '("/Users/doyley/Dropbox/Filing_Cabinet/B/refs/ref.bib")) ;; location
(setq helm-bibtex-library-path "~/Dropbox/Filing_Cabinet/B/refs/pdf")      ;; store pdf files associated with bibtex
(setq helm-bibtex-notes-path "~/Dropbox/Filing_Cabinet/B/refs/helm-bibtex-notes")
(setq  org-directory org-files )  ;; Directory where I store my org-files


(provide 'pirl-users)
