(require 'package)

(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(require 'diminish)
(require 'bind-key)

(setenv "PATH" "/usr/local/bin:/Library/TeX/texbin/:$PATH" t)
(org-babel-load-file (concat user-emacs-directory "pirl-config.org"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(epg-gpg-program "/usr/local/MacGPG2/bin/gpg2")
 '(org-latex-text-markup-alist
   (quote
    ((bold . "\\textbf{%s}")
     (code . verb)
     (italic . "\\emph{%s}")
     (strike-through . "\\textcolor{red}{%s}")
     (underline . "\\uline{%s}")
     (verbatim . protectedtexttt))))
 '(package-selected-packages
   (quote
    (synosaurus wc-mode org-sticky-header org-gcal org-ref which-key google-this smart-mode-line smex hydra python-mode elpy pylint ob-ipython theme-looper flatland-theme solarized-theme leuven-theme atom-dark-theme material-theme zenburn-theme sublime-themes spacemacs-theme palimpsest thesaurus magit golden-ratio yasnippet help-fns+ helm-projectile helm-bibtex flx f s dash-functional ox-reveal htmlize helm-swoop ov helm aggressive-indent real-auto-save highlight-parentheses beacon autopair wrap-region auctex go-autocomplete auto-complete use-package org-plus-contrib))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
