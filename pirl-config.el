
(setq abbrev-file-name    "~/Emacs/abbrev_defs")
(setenv "PATH" "/usr/local/bin:/Library/TeX/texbin:$PATH" t)

(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode 1)           ; keep the menus
(column-number-mode 1)
(mouse-wheel-mode t)
(show-paren-mode 1)
(global-font-lock-mode 1)
(blink-cursor-mode 0)  ; turn off blinking cursor
(transient-mark-mode 1) ;; No region when it is not highlighted

(display-time)
(fset 'yes-or-no-p 'y-or-n-p)
(add-hook 'text-mode-hook 'turn-on-auto-fill)

(setq warning-suppress-types '((python)
                               (emacs)))

(set-default-font "Inconsolata 24") ; Fonts

(setq-default tab-width 4)
(setq-default tab-stop-list (list 4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68 72 76 80 84 88 92 96 100 104 108))
(setq-default save-place t)
(setq-default line-spacing 0.25) ; add 0.5 height between lines
(setq-default cursor-type 'box)
(setq-default indent-tabs-mode nil);               Introduce spaces instead of tabs
(setq-default fill-column 100);
(setq-default c-basic-offset 4 c-default-style "linux")
(setq-default tab-width 4)
(setq python-indent-offset 4)
(when (string= system-type "darwin")       
  (setq dired-use-ls-dired nil))
(setq delete-by-moving-to-trash t
        inhibit-startup-message t;                  Inhibit startup message
        require-final-newline t ;                    Make the last line end in a carriage
        search-highlight t;                               Highlight incremental search
        default-major-mode 'text-mode;              Make text-mode default.
        mac-allow-anti-aliasing t
        initial-scratch-message ""
        transient-mark-mode t;
        comment-style 'plain
    org-src-tab-acts-natively t
        smex-save-file "~/Emacs/smex.save"
        fixed-width-rescale nil
     make-backup-files nil;                      Prevents emacs from making backup files
     use-home (concat (expand-file-name "~") "/")
     fill-column nil
     frame-title-format (list '("emacs ") '(buffer-file-name "%f" (dired-directory dired-directory "%b")))
     mac-option-modifier 'super
     mac-command-modifier 'meta
     initial-major-mode 'text-mode
     tramp-default-method "ssh"
     magit-last-seen-setup-instructions "1.4.0"
     helm-locate-fuzzy-match t
     wg-prefix-key (kbd "C-c z")
     max-lisp-eval-depth 10000
     max-specpdl-size 10000
     comint-process-echoes t
    ad-redefinition-action 'accept
    sentence-highlight-mode nil
    backup-by-copying-when-mismatch t
    ido-use-filename-at-point 'guess
    x-select-enable-clipboard t
)

        (setq auto-mode-alist
        '(("\\.c\\'" . c-mode) ("\\.h\\'" . c++-mode) 
        ("\\.bib$" . bibtex-mode)  ("\\.f\\'" . fortran-mode) ("\\.F\\'" . fortran-mode)("\\.f90\\'" . fortran-mode)
        ("\\.for\\'" . fortran-mode) ("\\.s?html?\\'" . html-helper-mode) ("\\.org\\'" . org-mode)
        ("\\.cpp\\'" . c++-mode) ("\\.hh\\'" . c++-mode) ("\\.cc\\'" . c++-mode) ("\\.C\\'" . c++-mode)
        ("\\.H\\'" . c++-mode) ("\\.cpp\\'" . c++-mode) ("\\.hxx\\'" . c++-mode) ("\\.c\\+\\+\\'" . c++-mode)
        ("\\.txt\\'" . text-mode)
        ("\\.sty\\'" . LaTeX-mode) ("\\.cls\\'" . LaTeX-mode) ("\\.clo\\'" . LaTeX-mode) ("\\.bbl\\'" . LaTeX-mode)
        ("\\.el\\'" . lisp-mode)
        ("\\.py\\'" . python-mode)
     ("\\.rst'" . rst-mode)
))

;; ispell
(setq ispell-program-name "/usr/local/bin/ispell"
          ispell-process-directory (expand-file-name "~/")
      ispell-parser 'tex
      ispell-personal-dictionary "~/Emacs/dict")


;;(define-key c-mode-base-map (kbd "RET") 'newline-and-indent)

 (setq ibuffer-shrink-to-minimum-size t
       ibuffer-always-show-last-buffer nil
       ibuffer-sorting-mode 'recency
       ibuffer-use-header-line t)

(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

;; Org-mode and related packages 
(use-package org-plus-contrib
  :mode (("\\.org$" . org-mode))
  :ensure t
 :init
  ;; Use the current window for C-c ' source editing
  (setq org-src-window-setup 'current-window
        org-support-shift-select t)
  ;; I like to press enter to follow a link. mouse clicks also work.
  (setq org-return-follows-link t)
  :bind (("\C-c a" . org-agenda);            Invoke org-agenda view
("\C-c l" . org-store-link)
("\C-c r" . org-bibtex)
("\C-c g" . org-mac-grab-link)
([f11] . org-beamer-set-environment-tag))
  :config
  (progn
(require 'org-caputure)
(setq org-hide-block-startup t)  ;; folding
(setq  org-reverse-note-order t)
(setq  org-enable-priority-commands t)
(setq  org-export-with-LaTeX-fragments t)
(setq  org-hide-leading-stars t);                                            hide leading star
(setq  org-blank-before-new-entry (quote ((heading) (plain-list-item))));    do not insert a blank
(setq  org-special-ctrl-k t);                                                prevent killing of whole
(setq  org-return-follows-link t);                                           ENTER opens links
(setq  org-alphabetical-lists t)
(setq org-src-fontify-natively nil)
(setq  org-agenda-skip-scheduled-if-done t)
(setq  org-agenda-todo-list-sublevels t) ;; Whether to check sublevels
(setq  org-latex-create-formula-image-program 'imagemagick)
(setq  org-latex-with-hyperref nil)
(setq  org-startup-with-inline-images "inlineimages") ;; default with images open
(setq  org-refile-targets (quote ((nil :maxlevel . 9) (org-agenda-files :maxlevel . 9))))
(setq  org-refile-use-outline-path  'file)
(setq  org-loop-over-headlines-in-active-region t)
(setq  org-outline-path-complete-in-steps t)
(setq  org-refile-allow-creating-parent-nodes (quote confirm))
(setq  org-agenda-start-on-weekday nil) ;; start on current day
(setq  org-confirm-babel-evaluate nil))
(setq  org-image-actual-width '(300))
(setq  org-agenda-include-diary t)
(setq  org-babel-python-command "python")
(setq  org-deadline-warning-days 10)
(setq  org-export-latex-listings 'minted)
(setq  org-agenda-span (quote fortnight))
(setq  org-agenda-skip-scheduled-if-deadline-is-shown t)
(setq  org-agenda-timegrid-use-ampm 1)
(setq  org-agenda-start-on-weekday 0)
(setq  org-catch-invisible-edits 'error)
(setq  org-tags-match-list-sublevels 'indented)
(setq  org-latex-listings 'minted)
(setq  org-latex-minted-options
                   '(("frame" "none")
                     ("fontsize" "\\scriptsize")
                     ("linenos" "")))

(setq org-export-latex-custom-lang-environments
                      '(
                    (emacs-lisp "common-lispcode")
                       )        
  org-startup-truncated nil)
)

;; org-mode Speed key
(setq org-use-speed-commands t);               Speed keys
 (setq org-speed-commands-user (quote (("d" . delete-window)
 ("0" . delete-other-windows)
 ("b" . org-tree-to-indirect-buffer)
 ("r" . org-refile)
 ("4" . org-reveal)
 ("k" . org-cut-subtree)
 )))

(prefer-coding-system 'utf-8)
(set-charset-priority 'unicode)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))
(setq helm-org-rifle-show-path t)
(setq real-auto-save-interval 300) ;;            in seconds
(global-set-key [(super h)] 'ov-highlighter/body)
(setq diary-file "~/Org-files/Tickler")
(add-hook 'org-mode-hook 'flyspell-mode)

(setq org-export-latex-minted-options
'(;("frame" "single")
  ("bgcolor" "bg") ;  It's defined in org-preamble-pdflatex.sty and org-preamble-xelatex.sty below.
  ("fontsize" "\\small")
    ))
(setq org-agenda-time-grid '((daily today today)
                             #("----------------" 0 16 (org-heading t))
                             (800 1000 1200 1400 1600 1800 2000)))
(setq org-agenda-todo-list-sublevels t) ;; Whether to check sublevels
(setq org-export-latex-emphasis-alist (quote (("*" "\\textbf{%s}" nil)
("/" "\\emph{%s}" nil) ("_" "\\alert{%s}" nil) ("+" "\\st{%s}" nil)
("=" "\\verb" t) (" <at> " "\\alert{%s}" nil) ("!" "\\note{%s}" nil) ("~"
"\\verb" t))))

;; formatting adgenda view
(setq org-agenda-prefix-format '((todo . "   %b")))
(setq org-format-latex-options (plist-put org-format-latex-options :scale 4.0))

;; Osx Manuscripts
(use-package ox-manuscript
 :load-path "lisp/")

;; Capture template
  (setq org-capture-templates
      '(("i" "Inline Notes" entry (file+headline (buffer-file-name (org-capture-get :original-buffer)) "Log")
             "* TODO %^{Task}" :prepend t)
        ("b" "Brain" entry (file+headline (concatenate 'string org-files brain) "Notes")
             "* %^{title} %^g\n" :prepend t)
        ("t" "todo" entry (file+headline (conatenate 'string org-files mytodo) "Inbox")
             "* TODO [#A] %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n" :prepend t)
        ("l" "logs" entry (file+headline (concatenate 'string org-files brain) "Logs")
             "* %^{title} %^g\n" :prepend t)
        ("j" "journal" entry (file+headline (concatenate 'string org-files brain) "Blog")
             "* %^{title} %^g\n" :prepend t)
    ))

;; Define user specific files. This is the only file that need to be configured
(use-package pirl-users
  :load-path "users/")

;; Auto-complete
(use-package auto-complete-config)
(use-package auto-complete
  :ensure t
  :diminish auto-complete-mode
  :config
  (progn
    (use-package go-autocomplete :ensure t)
     (setq ac-use-fuzzy t
        ac-disable-inline t
        ac-use-menu-map t
        ac-auto-show-menu t
        ac-auto-start 2
        ac-ignore-case t
        ac-menu-height 50
        ac-sources '(ac-source-semantic ac-source-yasnippet)
        ac-dwim t
        ac-quick-help-delay 1
        ac-quick-help-height 60
        ac-disable-inline t
        ac-show-menu-immediately-on-auto-complete t
        ac-candidate-menu-min 0)
        (set-default 'ac-sources
             '(ac-source-dictionary
               ac-source-words-in-buffer
               ac-source-words-in-same-mode-buffers
               ac-source-semantic
               ac-source-yasnippet))

    (add-to-list 'ac-modes 'org-mode)
    (add-to-list 'ac-modes 'text-mode))
  :bind (("C-n" . ac-next)
         ("C-p" . ac-previous))
    )
(global-auto-complete-mode t)
(dolist (mode '(magit-log-edit-mode log-edit-mode org-mode text-mode haml-mode
                sass-mode yaml-mode csv-mode f90-mode haskell-mode
                html-mode nxml-mode sh-mode python-mode 
                lisp-mode textile-mode markdown-mode tuareg-mode))
  (add-to-list 'ac-modes mode))

(use-package tex
  :ensure auctex
  :mode ("\\.tex\\'" . LaTeX-mode)
)

;; wrap region
(use-package wrap-region
  :ensure t)

;; automatically  insert parentheses, braces, quotes and the like in matching pairs
(use-package autopair
  :ensure t
  :diminish autopair-mode
  :config (autopair-global-mode 1)
  )

(use-package beacon
  :ensure t
  :diminish beacon-mode
  :config
  (setq beacon-push-mark 35)
  (setq beacon-color "#666600")
  (beacon-mode 1))

(use-package highlight-parentheses
  :ensure t
  :commands highlight-parentheses-mode
  :diminish highlight-parentheses-mode)

(use-package bookmark
  :init
  (setq bookmark-default-file (expand-file-name "bookmarks" "~/Emacs/")
        bookmark-save-flag 1))

(use-package real-auto-save
   :ensure t)

(use-package aggressive-indent
 :ensure t
 :config (aggressive-indent-global-mode 1))

;Cut and past with mouse
(use-package mouse-copy
   :ensure t)

;; Helm 
(use-package helm
  :diminish helm-mode 
  :ensure t
  :config 
  (require 'helm-config)
  (require 'helm-eshell)
  (require 'helm-files)
  (require 'helm-grep)
  ;;(require 'helm-org-rifle)
  :init
  (helm-mode 1)
 (global-set-key (kbd "C-x b")   #'helm-mini)
        (global-set-key (kbd "C-x C-b")   #'helm-buffers-list)
        (global-set-key (kbd "C-x C-m")  #'helm-M-x)
        (global-set-key (kbd "M-y")     #'helm-show-kill-ring)
    :bind (("M-x"  . helm-M-x)
        ("M-y"  . helm-show-kill-ring)
        ("C-x C-f"  . helm-find-files)
        ("C-x b"  . helm-mini)
        ("C-x C-r"  . helm-recentf)
        ("C-x r l"  . helm-bookmarks)
        ("C-c h s"  . helm-semantic-or-imenu)
        ("C-c h m"  . helm-man-woman)
        ("C-c h f"  . helm-find)
        ("C-c h l"  . helm-locate)
        ("C-c h o"  . helm-occur)
        ("C-c h r"  . helm-resume)
        ("C-c m" . helm-all-mark-rings)        
                 (:map helm-map
                          ("C-z"  . helm-select-action)
                          ("C-x 2" . helm-select-2nd-action)
                          ("C-x 3" . helm-select-3rd-action)
                          ("C-x 4" . helm-select-4rd-action))
                    (:map  helm-grep-mode-map
                           ("<return>"  . helm-grep-mode-jump-other-window)
                           ("n"  . helm-grep-mode-jump-other-window-forward)
         ("p" . helm-grep-mode-jump-other-window-backward))))
;;* Helm
;; http://tuhdo.github.io/helm-intro.html
(setq helm-command-prefix-key "C-c h")
(autoload 'helm-bibtex "helm-bibtex" "" t)

(use-package ov
  :ensure t)

;; Helm swoop, fancy search
(use-package helm-swoop
 :ensure t
 :bind (("C-S-s" . helm-swoop)
  ("M-i" . helm-swoop)
  ("C-s " . helm-swoop)         
  ("M-s M-s" . helm-swoop)
  ("M-I" . helm-swoop-back-to-last-point)
  ("C-c M-i" . helm-multi-swoop)
  ("C-x M-i" . helm-multi-swoop-all)
  )
:config
 (progn
   (define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)
   (define-key helm-swoop-map (kbd "M-i") 'helm-multi-swoop-all-from-helm-swoop))
)

(use-package htmlize
  :ensure t)

;; Ispell
(use-package  ispell
 :ensure t)

(use-package  dash-functional
 :ensure t)
 (use-package  dash
 :ensure t)
 (use-package  s
 :ensure t)
 (use-package  f
 :ensure t)
(use-package flx
 :ensure t)
(use-package helm-bibtex
  :ensure t)

(use-package helm-projectile
 :ensure t)
(use-package help-fns+
 :ensure t)

(use-package recentf
  :ensure t
  :init
  (recentf-mode 1)
  (setq recentf-max-saved-items 50)
)

(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1)
 (defun yas/org-very-safe-expand ()
   (let ((yas/fallback-behavior 'return-nil)) (yas/expand)))
  )

(use-package eshell
  :ensure t)

;; Scale windows using the golden rule
(use-package golden-ratio
  :ensure t
  :diminish golden-ratio-mode
  :init
  (golden-ratio-mode 1))

(use-package magit
  :ensure t
  :init (setq magit-completing-read-function 'ivy-completing-read)
  :bind
  ("<f5>" . magit-status)
  ("C-c v t" . magit-status))

(use-package flyspell
  :ensure t
  :bind ([(control .)] . flyspell-auto-correct-word)
 )

;; setting up cdlatex
(use-package cdlatex
  :load-path "~/.emacs.d/lisp")
;; Redefinding cdlatex symbols 
 (setq cdlatex-math-symbol-alist
  '((?t "\\theta" "\\tau" "\\tan" )
    (?e "\\varepsilon")
    (?n "\\nabla" "\\nu")
    (?p "\\pi" "\\partial")
    (?N "\\nu")
    (?s "\\sigma" "\\sigma^2" "\\sin")
    (?. "\\cdot")
    (?* "\\times")
    (?l "\\lambda" "\\ln")
    (?/ "\\displaystyle")
    (?% "\\%")
    (?% "\\times")
  ))

(use-package thesaurus
:ensure t
:init
    (setq thesaurus-bhl-api-key "706a11ce26c3c2baf1e2e5ee9d76cd5f"))

;; move text to top or bottom of file
(use-package palimpsest
:ensure t)
(add-hook 'org-mode-hook 'palimpsest-mode)
(add-hook 'text-mode-hook 'palimpsest-mode)

;; Abbrev mode
(use-package abbrev
  :diminish abbrev-mode
  :config
  (progn (abbrev-mode)
         (setq abbrev-file-name "~/Emacs/abbrev_defs")))

(setq solarized-scale-org-headlines nil)
(setq solarized-use-less-bold t)
(setq solarized-emphasize-indicators nil)
(setq solarized-use-variable-pitch nil)
(setq solarized-distinct-fringe-background t)

(setq solarized-height-minus-1 1.0)
(setq solarized-height-plus-1 1.0)
(setq solarized-height-plus-2 1.0)
(setq solarized-height-plus-3 1.0)
(setq solarized-height-plus-4 1.0)

(setq org-fontify-whole-heading-line t)
(setq leuven-scale-outline-headlines nil)
(use-package spacemacs-theme
 :ensure t
 :defer t)
(use-package sublime-themes
  :ensure t
  :defer t)
(use-package zenburn-theme
 :ensure t
 :defer t)
(use-package material-theme
 :ensure t
 :defer t)
(use-package atom-dark-theme
 :ensure t
 :defer t)
(use-package leuven-theme
 :ensure t)
(use-package solarized-theme
 :ensure t)
 :config
;;   (load-theme 'solarized-dark t))

(use-package flatland-theme
 :ensure t
 :defer t)

(use-package theme-looper
  :ensure t
  :config (theme-looper-set-theme-set (list 'spacemacs-dark 'zenburn 'material 'atom-dark 'leuven 
  'flatland 'solarized-dark 'solarized-light 'dorsey 'material-light))
  :bind ("C-\"" . theme-looper-enable-next-theme)
)

(use-package ob-ipython
  :ensure t
 :config
  (setq org-confirm-babel-evaluate nil)   ;don't prompt me to confirm everytime I want to evaluate a block
  (add-hook 'org-babel-after-execute-hook 'org-display-inline-images 'append)
)

;; Org-babel
(org-babel-do-load-languages
 'org-babel-load-languages
'((emacs-lisp . t)
   (lisp . t)
   (fortran . t)
   (C . t)
   (org . t)
   (latex . t)
   (ipython . t)
   (python . t)
   ))

;; Org babel snippets
;; add <p for python expansion
(add-to-list 'org-structure-template-alist
             '("p" "#+BEGIN_SRC python\n?\n#+END_SRC" "<src lang=\"python\">\n?\n</src>"))

;; add <por for python expansion with raw output
(add-to-list 'org-structure-template-alist
             '("por" "#+BEGIN_SRC python :results output raw\n?\n#+END_SRC" "<src lang=\"python\">\n?\n</src>"))

;; add <pv for python expansion with value
(add-to-list 'org-structure-template-alist
             '("pv" "#+BEGIN_SRC python :results value\n?\n#+END_SRC" "<src lang=\"python\">\n?\n</src>"))
;; add response bolck
(add-to-list 'org-structure-template-alist
             '("r" "#+BEGIN_response\n?\n#+END_response" ""))
;; add solution block when writing exams
(add-to-list 'org-structure-template-alist
             '("s" "#+BEGIN_solution\n?\n#+END_solution" ""))


;; add <el for emacs-lisp expansion
(add-to-list 'org-structure-template-alist
             '("el" "#+BEGIN_SRC emacs-lisp\n?\n#+END_SRC" "<src lang=\"emacs-lisp\">\n?\n</src>"))

;; add <sh for shell
(add-to-list 'org-structure-template-alist
             '("sh" "#+BEGIN_SRC sh\n?\n#+END_SRC" "<src lang=\"shell\">\n?\n</src>"))

(add-to-list 'org-structure-template-alist
             '("lh" "#+latex_header: " ""))

(add-to-list 'org-structure-template-alist
             '("lc" "#+latex_class: " ""))

(add-to-list 'org-structure-template-alist
             '("lco" "#+latex_class_options: " ""))

(add-to-list 'org-structure-template-alist
             '("ao" "#+attr_org: " ""))

(add-to-list 'org-structure-template-alist
             '("al" "#+attr_latex: " ""))

(add-to-list 'org-structure-template-alist
             '("ca" "#+caption: " ""))

(add-to-list 'org-structure-template-alist
             '("tn" "#+tblname: " ""))

(add-to-list 'org-structure-template-alist
             '("n" "#+name: " ""))

(use-package ox-ipynb
  :load-path "lisp/")

;; Grab links on mac
 (use-package org-mac-link)

(use-package animate :ensure t)
(use-package org-show
 :load-path "lisp/")

(with-eval-after-load 'org       
  (setq org-startup-indented t) ; Enable `org-indent-mode' by default
  (add-hook 'org-mode-hook #'visual-line-mode))

;; Smart modeline
(use-package smart-mode-line
  :ensure t
  :disabled
  :config
  (setq sml/no-confirm-load-theme t)
  (setq sml/theme 'light)
  (sml/setup))

(use-package pylint
 :ensure t)

(use-package py-autopep8
 :disabled
 :ensure t)

;; Git interface.
(use-package magit
  :ensure t
  :diminish auto-revert-mode
  :commands (magit-status magit-checkout)
  :bind (("C-x g" . magit-status))
  :init
  (setq magit-revert-buffers 'silent
        magit-push-always-verify nil
        git-commit-summary-max-length 70))

(use-package cdlatex)

;; Python editing mode
(use-package elpy
  :ensure t
  :config
  (elpy-enable))

(use-package python-mode
  :ensure t
)
(setenv "PYTHONPATH" "/Users/doyley/Dropbox/Filing_Cabinet/P/Python/")
(setq python-shell-native-complete nil)

(use-package pirl-mode
  :load-path "~/.emacs.d/lisp/"
)

(use-package hydra
  :ensure t
  :init
  (setq hydra-is-helpful t)

  :config
  (require 'hydra-ox))

(use-package scimax-org-babel-python
 :load-path "lisp/")
(use-package scimax-org-babel-ipython
 :load-path "lisp/")

(add-to-list 'org-ctrl-c-ctrl-c-hook 'org-babel-async-execute:python)

;; keep recent commands available in M-x
(use-package smex
 :ensure t)

(use-package smart-mode-line
  :ensure t
  :config
  (setq sml/no-confirm-load-theme t)
  (setq sml/theme 'light)
  (sml/setup))

;; Org edit marks
(use-package org-editmarks
 :load-path "lisp/")

(use-package ido
  :ensure t 
  :init
  (ido-mode 1)
   (setq
      ido-enable-flex-matching t
          ido-everywhere t  
          ido-ignore-buffers               ; ignore these guys
                '("\\` " "^\*Mess" "^\*Back" ".*Completion" "^\*Ido")
          ido-work-directory-list '("~/" "~/Emacs")
          ido-case-fold  t                 ; be case-insensitive
          ido-use-filename-at-point nil    ; don't use filename at point (annoying)
          ido-use-url-at-point nil         ; don't use url at point (annoying)
          ido-enable-flex-matching t       ; be flexible
          ido-max-prospects 6              ; don't spam my minibuffer
          ido-confirm-unique-completion t
      ido-separator "\n"
      ido-enable-flex-matching t
    ))

(use-package starttls
 :defer t
 :init 
(setq starttls-use-gnutls t)
(setq user-mail-address "mdoyley@ur.rochester.edu")
(setq user-full-name "Doyley, Marvin"))
(use-package smtpmail
  :init 
          (setq
           send-mail-function 'smtpmail-send-it
           message-send-mail-function 'smtpmail-send-it
           user-mail-address "mdoyley@ur.rochester.edu"
           user-full-name "Doyley, Marvin"
           smtpmail-starttls-credentials '(("outlook.office365.com" 587 nil nil))
           smtpmail-auth-credentials  (expand-file-name "~/.authinfo.gpg")
           smtpmail-default-smtp-server "outlook.office365.com"
           smtpmail-smtp-server "outlook.office365.com"
           smtpmail-smtp-service 587
           smtpmail-stream-type 'starttls))

(use-package google-this
 :ensure t
 :init
  (google-this-mode 1)
 )

(use-package spaceline-config
 :commands spaceline-emacs-theme
 :init
 (setq spaceline-separator-dir-left '(left . left))
 (setq spaceline-separator-dir-right '(right . right))
  )

(use-package elpy
  :ensure t)
(elpy-enable)
(setq elpy-rpc-backend "rope")
(setq elpy-rpc-python-command "/anaconda/bin/python")
(define-key yas-minor-mode-map (kbd "C-C k") 'yas-expand)
(define-key global-map (kbd "C-C o") 'iedit-mode)

(use-package which-key
  :ensure t
  :init (which-key-mode)
)

;; Artbollocks mode for correcting writing
(use-package artbollocks-mode
 :load-path "Artbollocks/")
(add-hook 'org-mode-hook 'artbollocks-mode)

;; Hotspot utility developed by John kitchin
(defcustom scimax-user-hotspot-commands '()
  "A-list of hotspots to jump to in `hotspots'.
These are shortcut to commands.
\(\"label\" . command)")

(defcustom scimax-user-hotspot-locations '()
  "A-list of hotspot locations to jump to in  `hotspots'.
\(\"label\" . \"Path to file\").
These are like bookmarks.")
(defun hotspots (arg)
  "Helm interface to hotspot locations.
This includes user defined
commands (`scimax-user-hotspot-commands'),
locations (`scimax-user-hotspot-locations'), org agenda files,
recent files and bookmarks. You can set a bookmark also."
  (interactive "P")
  (helm :sources `(((name . "Commands")
                    (candidates . ,scimax-user-hotspot-commands)
                    (action . (("Open" . (lambda (x) (funcall x))))))
                   ((name . "My Locations")
                    (candidates . ,scimax-user-hotspot-locations)
                    (action . (("Open" . (lambda (x) (find-file x))))))
                   ((name . "My org files")
                    (candidates . ,org-agenda-files)
                    (action . (("Open" . (lambda (x) (find-file x))))))
                   helm-source-recentf
                   helm-source-bookmarks
                   helm-source-bookmark-set)))


(add-to-list 'safe-local-eval-forms 
             '(progn (require 'emacs-keybinding-command-tooltip-mode) (emacs-keybinding-command-tooltip-mode +1)))

(setq scimax-user-hotspot-commands
      '(
        ("Calendar" . (lambda ()
                        (browse-url "https://calendar.google.com/calendar/render#main_7%7Cday")))
        ("Agenda" . (lambda () (org-agenda "" "W"))) 
    ("Projects". (lambda () 
                (dired 
                    "~/Org-files")))
        ("Brain" . (lambda ()
                  (find-file
                   "/Users/doyley/Org-files/Reference/Brain.org")))))

;; Creates electronic notebook using projectile
(use-package scimax-notebook
 :load-path "lisp/")

(use-package org-ref
 :ensure t
 :config
   (require 'org-ref-isbn)
   (require 'org-ref-pubmed)
   (require 'org-ref-arxiv)
   (require 'org-ref-bibtex)
   (require 'org-ref-pdf))

(setq helm-bibtex-pdf-open-function
  (lambda (fpath)
    (start-process "open" "*open*" "open" fpath)))

;; Reftex
(use-package reftex
  :ensure  t
  :commands turn-on-reftex
  :init
  (progn
    (setq reftex-plug-into-AUCTeX t)))


;; Bibtex mode
(use-package bibtex
  :ensure t
  :mode ("\\.bib" . bibtex-mode)
  :init
  (progn
    (setq bibtex-align-at-equal-sign t)
    (add-hook 'bibtex-mode-hook (lambda () (set-fill-column 120)))))

(use-package python
  :ensure t
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode)
  :init
  (eval-after-load 'python
    (lambda ()
      (defun python-shell-completion-native-try ()
        "Return non-nil if can trigger native completion."
        (let ((python-shell-completion-native-enable t)
              (python-shell-completion-native-output-timeout
               python-shell-completion-native-try-output-timeout))
          (python-shell-completion-native-get-completions
           (get-buffer-process (current-buffer))
           nil "_"))))))

;; Encryption
(use-package org-crypt
  :defer t
  :init
  (setq epg-gpg-program "gpg2")
  (setq org-crypt-key nil)
  (setq org-tags-exclude-from-inheritance (quote ("crypt")))
  :commands  (org-crypt-use-before-save-magic)
  )

(use-package org-gcal
    :ensure t)

(setq org-gcal-client-id "753306665930-eolr2a7l0kfmjl944mj9nu7bnigrbdfn.apps.googleusercontent.com"   
      org-gcal-client-secret "5ukYMTk_-NO4BDcJf5ivWIOB"
      org-gcal-file-alist '(("4c9ohtq09ibvcr45eobi7d22d4@group.calendar.google.com" .  "~/Org-files/google.org")))

(defun org-gcal--notify (title mes)
(let ((file (expand-file-name (concat (file-name-directory
(locate-library "org-gcal")) org-gcal-logo)))
(mes mes)
(title title))
(if (eq system-type 'gnu/linux)
(progn
(if (not (file-exists-p file))
(deferred:$
(deferred:url-retrieve (concat "
https://raw.githubusercontent.com/myuhe/org-gcal.el/master/" org-gcal-logo))
(deferred:nextc it
(lambda (buf)
(with-current-buffer buf
(let ((tmp (substring (buffer-string) (+
(string-match "\n\n" (buffer-string)) 2))))
(erase-buffer)
(fundamental-mode)
(insert tmp)
(write-file file)))
(kill-buffer buf)))))
(alert mes :title title :icon file))
(alert mes :title title))
))

(use-package words
  :load-path "lisp/")

(use-package org-sticky-header
  :ensure t)

(use-package wc-mode
  :ensure t)
(global-set-key "\C-cw" 'wc-mode)

;; Uses wordnet as backend for synonums
(use-package synosaurus
  :ensure t
  :commands synosaurus-choose-and-replace
  :init (setq synosaurus-choose-method 'popup)
  :config
  (bind-key "C-x t" 'synosaurus-choose-and-replace text-mode-map))

(add-hook 'text-mode-hook 'synosaurus-mode)
(add-hook 'org-mode-hook 'synosaurus-mode)

(use-package popup
  :ensure t)

(defun new-log-entry ()
  "Create and load a journal file based on today's date."
  (interactive)
  (find-file (get-data-file)))
 
;; Creating index of directory of org-files
(defun org-toc ()
  "Generate a table of contents for org-files in this directory."
  (interactive)
  (let ((org-agenda-files (f-entries "." (lambda (f) (f-ext? f "org")) t)))
    (helm-org-agenda-files-headings)))

(defun marv:insert-date ()
  "Insert date at point."
  (interactive)
  (insert (format-time-string "%A, %B %e, %Y %k:%M:%S")))

(defun marv:create-scratch-buffer nil
       "create a scratch buffer"
       (interactive)
       (switch-to-buffer (get-buffer-create "*scratch*"))
       (lisp-interaction-mode))

(defun org-babel-python-strip-session-chars ()
  "Remove >>> and ... from a Python session output."
  (when (and (string=
              "python"
              (org-element-property :language (org-element-at-point)))
             (string-match
              ":session"
              (org-element-property :parameters (org-element-at-point))))

    (save-excursion
      (when (org-babel-where-is-src-block-result)
        (goto-char (org-babel-where-is-src-block-result))
        (end-of-line 1)
        ;(while (looking-at "[\n\r\t\f ]") (forward-char 1))
        (while (re-search-forward
                "\\(>>> \\|\\.\\.\\. \\|: $\\|: >>>$\\)"
                (org-element-property :end (org-element-at-point))
                t)
          (replace-match "")
          ;; this enables us to get rid of blank lines and blank : >>>
          (beginning-of-line)
          (when (looking-at "^$")
            (kill-line)))))))
            

;; Sync Google calendar
(defun sync-google-calendar ()
  "Save google calender entries into dairy.
See more about this approach at Https://www.youtube.com/watch?v=cIzzjSaq2N8&t=339s"
  (interactive)
  (call-process "~/Org-files/get_ical.py" nil 0 nil)
  (mapcar (lambda (icsfile)
             (icalendar-import-file icsfile "~/Org-files/Tickler")
             ) (file-expand-wildcards "~/Org-files/*.ics"))

  )

(defun org-begin-template ()
  "Make a template at point."
  (interactive)
  (if (org-at-table-p)
      (call-interactively 'org-table-rotate-recalc-marks)
    (let* ((choices '(("s" . "SRC")
                      ("e" . "EXAMPLE")
                      ("q" . "QUOTE")
                      ("v" . "VERSE")
                      ("c" . "CENTER")
                      ("l" . "LaTeX")
                      ("h" . "HTML")
                      ("a" . "ASCII")))
           (key
            (key-description
             (vector
              (read-key
               (concat (propertize "Template type: " 'face 'minibuffer-prompt)
                       (mapconcat (lambda (choice)
                                    (concat (propertize (car choice) 'face 'font-lock-type-face)
                                            ": "
                                            (cdr choice)))
                                  choices
                                  ", ")))))))
      (let ((result (assoc key choices)))
        (when result
          (let ((choice (cdr result)))
            (cond
             ((region-active-p)
              (let ((start (region-beginning))
                    (end (region-end)))
                (goto-char end)
                (insert "#+END_" choice "\n")
                (goto-char start)
                (insert "#+BEGIN_" choice "\n")))
             (t
              (insert "#+BEGIN_" choice "\n")
              (save-excursion (insert "#+END_" choice))))))))))

(defun org-auto-capitalize-headings-and-lists ()
  "Create a buffer-local binding of sentence-end to auto-capitalize
section headings and list items."
  (make-local-variable 'sentence-end)
  (setq sentence-end (concat (rx (or
                                  ;; headings
                                  (seq line-start (1+ "*") (1+ space))
                                  ;; list and checklist items
                                  (seq line-start (0+ space) "-" (1+ space) (? (or "[ ]" "[X]") (1+ space)))))
                             "\\|" (sentence-end))))



(setq org-latex-pdf-process
  '("latexmk -pdflatex='pdflatex -interaction nonstopmode' -pdf -bibtex -f %f"))
;; xelatex

(add-hook 'LaTeX-mode-hook (lambda ()
  (push
    '("pdlatex" "%`pdflatex%(mode)%' %t"TeX-run-TeX nil t)
    TeX-command-list)))

(setq TeX-save-query nil)
(org-add-link-type
 "latex" nil
 (lambda (path desc format)
   (cond
    ((eq format 'html)
     (format "<span class=\"%s\">%s</span>" path desc))
    ((eq format 'latex)
     (format "\\%s{%s}" path desc)))))

(defun get-data-file()
  (let ((name (read-string "Name: ")))
    (expand-file-name (format "%s-%s.org"
                              (format-time-string "%Y-%m-%d")
                              name) "~/Dropbox/Filing_Cabinet/Notes/")))
;; specify the justification you want
(plist-put org-format-latex-options :justify 'center)

(defun org-justify-fragment-overlay (beg end image imagetype)
  "Adjust the justification of a LaTeX fragment.
The justification is set by :justify in
`org-format-latex-options'. Only equations at the beginning of a
line are justified."
  (cond
   ;; Centered justification
   ((and (eq 'center (plist-get org-format-latex-options :justify)) 
         (= beg (line-beginning-position)))
    (let* ((img (create-image image 'imagemagick t))
           (width (car (image-size img)))
           (offset (floor (- (/ (window-text-width) 2) (/ width 2)))))
      (overlay-put (ov-at) 'before-string (make-string offset ? ))))
   ;; Right justification
   ((and (eq 'right (plist-get org-format-latex-options :justify)) 
         (= beg (line-beginning-position)))
    (let* ((img (create-image image 'imagemagick t))
           (width (car (image-display-size (overlay-get (ov-at) 'display))))
           (offset (floor (- (window-text-width) width (- (line-end-position) end)))))
      (overlay-put (ov-at) 'before-string (make-string offset ? ))))))

(defun org-latex-fragment-tooltip (beg end image imagetype)
  "Add the fragment tooltip to the overlay and set click function to toggle it."
  (overlay-put (ov-at) 'help-echo
               (concat (buffer-substring beg end)
                       "mouse-1 to toggle."))
  (overlay-put (ov-at) 'local-map (let ((map (make-sparse-keymap)))
                                    (define-key map [mouse-1]
                                      `(lambda ()
                                         (interactive)
                                         (org-remove-latex-fragment-image-overlays ,beg ,end)))
                                    map)))

;; advise the function 
(advice-add 'org--format-latex-make-overlay :after 'org-justify-fragment-overlay)
(advice-add 'org--format-latex-make-overlay :after 'org-latex-fragment-tooltip)
;;bind to key
(global-set-key (kbd "C-j")
            (lambda ()
                  (interactive)
                  (join-line -1)))
(defun smart-tab ()
â€œThis smart tab is minibuffer compliant: it acts as usual in
the minibuffer. Else, if mark is active, indents region. Else if
point is at the end of a symbol, expands it. Else indents the
current line.â€
(interactive)
(if (minibufferp)
(unless (minibuffer-complete)
(dabbrev-expand nil))
(if mark-active
(indent-region (region-beginning)
(region-end))
(if (looking-at â€œ\\_>â€)
(dabbrev-expand nil)
(indent-for-tab-command)))))

(defun org-toggle-iimage-in-org ()
  "display images in your org file"
  (interactive)
  (if (face-underline-p 'org-link)
      (set-face-underline-p 'org-link nil)
      (set-face-underline-p 'org-link t))
  (iimage-mode))


(defun raise-emacs-on-aqua()
(shell-command "osascript -e 'tell app \"Emacs.app\" to activate' &"))


(defun eshell-handle-ansi-color ()
  (ansi-color-apply-on-region eshell-last-output-start
                               eshell-last-output-end))
 (defun marv:yank-more ()
   (interactive)
   (insert "[[")
   (yank)
   (insert "][link-name]]"))

;; autocomplete

 (defadvice ac-start (before advice-turn-on-auto-start activate)
   (set (make-local-variable 'ac-auto-start) t))
 (defadvice ac-cleanup (after advice-turn-off-auto-start activate)
   (set (make-local-variable 'ac-auto-start) nil))

;; make file

(defun get-above-makefile () (expand-file-name
           "Makefile" (loop as d = default-directory then (expand-file-name
           ".." d) if (file-exists-p (expand-file-name "Makefile" d)) return
           d)))

        (add-hook 'fancy-diary-display-mode-hook
                   '(lambda ()
                     (alt-clean-equal-signs)))

(defun alt-clean-equal-signs ()
  "This function makes lines of = signs invisible."
        (goto-char (point-min))
        (let ((state buffer-read-only))
        (when state (setq buffer-read-only nil))
        (while (not (eobp))
        (search-forward-regexp "^=+$" nil 'move)
        (add-text-properties (match-beginning 0)
        (match-end 0)
        '(invisible t)))
        (when state (setq buffer-read-only t))))

(defun my-completion ()
  (interactive)
  (if (save-excursion
        (re-search-backward  "\\(.+/\\|\\(ls\\|cd\\|what\\|dir\\|addpath\\|load\\)[( ]+\\)" (point-at-bol) t))
      (comint-dynamic-complete-filename)
    (complete-tag)))


        ;; Slick copy, Copy without selecting
(defadvice kill-ring-save (before slick-copy activate compile)
  "When called interactively with no active region, copy a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (message "Copied line")
     (list (line-beginning-position)
      (line-beginning-position 2)))))

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-beginning-position 2)))))

                   (defun vsplit-last-buffer ()
                     "Split the window vertically and display the previous buffer."
                     (interactive)
                     (split-window-vertically)
                     (other-window 1 nil)
                     (switch-to-next-buffer))
                   (defun hsplit-last-buffer ()
                     "Split the window horizontally and display the previous buffer."
                     (interactive)
                     (split-window-horizontally)
                     (other-window 1 nil)
                     (switch-to-next-buffer))
(defun ido-recentf-open ()
  "Use `ido-completing-read' to find a recent file."
  (interactive)
  (if (find-file (ido-completing-read "Find recent file: " recentf-list))
      (message "Opening file...")
    (message "Aborting")))

        ;; Slick copy
(defadvice kill-ring-save (before slick-copy activate compile) "When called
  interactively with no active region, copy a single line instead."
  (interactive (if mark-active (list (region-beginning) (region-end)) (message
  "Copied line") (list (line-beginning-position) (line-beginning-position
  2)))))

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
    (if mark-active (list (region-beginning) (region-end))
      (list (line-beginning-position)
        (line-beginning-position 2)))))

;; Slick copy, Copy without selecting
(defadvice kill-ring-save (before slick-copy activate compile)
  "When called interactively with no active region, copy a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (message "Copied line")
     (list (line-beginning-position)
      (line-beginning-position 2)))))

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-beginning-position 2)))))

;; Slick copy
(defadvice kill-ring-save (before slick-copy activate compile) "When called
  interactively with no active region, copy a single line instead."
  (interactive (if mark-active (list (region-beginning) (region-end)) (message
  "Copied line") (list (line-beginning-position) (line-beginning-position
  2)))))

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
    (if mark-active (list (region-beginning) (region-end))
      (list (line-beginning-position)
        (line-beginning-position 2)))))

;; Slick copy, Copy without selecting
(defadvice kill-ring-save (before slick-copy activate compile)
  "When called interactively with no active region, copy a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (message "Copied line")
     (list (line-beginning-position)
      (line-beginning-position 2)))))

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(add-hook 'prog-mode-hook 'linum-mode)
(autoload 'browse-kill-ring "browse-kill-ring" t);                   Set up browse-kill-ring.  M-y invokes browse-kil
(setq system-uses-terminfo nil)
(define-key dired-mode-map "r" 'wdired-change-to-wdired-mode)
(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)
(eval-after-load "flyspell"
        '(define-key flyspell-mode-map [down-mouse-3] 'flyspell-correct-word))

    
   (add-hook 'org-babel-after-execute-hook
                   (lambda ()
                      (org-display-inline-images nil t)))
        (add-hook 'LaTeX-mode-hook 'flyspell-mode)
        (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode) ;turn on math-mode by default
        (add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
        (add-hook 'org-mode-hook 'turn-on-org-cdlatex)
        (add-hook 'latex-mode-hook 'turn-on-org-cdlatex)
        (add-hook 'org-agenda-before-write-hook 'org-agenda-add-entry-text)
        (add-hook 'org-mode-hook 'flyspell-mode)
    (add-hook 'org-mode-hook 'org-sticky-header-mode)
        (add-hook 'text-mode-hook (lambda () (abbrev-mode 1)))
        (add-hook 'org-mode-hook (lambda () (abbrev-mode 1)))    
        (add-hook 'fancy-diary-display-mode-hook
     '(lambda ()
                (alt-clean-equal-signs)))

        (add-hook 'org-mode-hook
                    (lambda ()
                      (make-variable-buffer-local 'yas/trigger-key)
                      (setq yas/trigger-key [tab])
                      (add-to-list 'org-tab-first-hook 'yas/org-very-safe-expand)
                      (define-key yas/keymap [tab] 'yas/next-field)))

(add-hook 'org-babel-after-execute-hook 'org-babel-python-strip-session-chars)
(add-hook 'org-babel-after-execute-hook 'org-display-inline-images 'append)   
(add-hook 'after-make-frame-functions 'fit-frame)

    ;; Shell mode
        (add-hook 'shell-mode-hook
                  'ansi-color-for-comint-mode-on)
        (add-hook 'shell-mode-hook 'turn-on-font-lock)
        ; make completion buffers disappear after 3 seconds.

        (add-hook 'completion-setup-hook
          (lambda () (run-at-time 3 nil
            (lambda () (delete-windows-on "*Completions*")))))


                (add-hook 'term-exec-hook
                          (function
                           (lambda ()
                             (set-buffer-process-coding-system 'utf-8-unix 'utf-8-unix))))
                (setq system-uses-terminfo nil)

                ;; remove trailing white space
                        (add-hook 'before-save-hook 'my-other-delete-trailing-blank-lines)


(setq lexical-illusions-regex "\\b\\(\\w+\\)\\W+\\(\\1\\)\\b")

;; Make sure keywords are case-insensitive
(defadvice search-for-keyword (around sacha activate)
  "Match in a case-insensitive way."
  (let ((case-fold-search t))
    ad-do-it))
(add-hook 'org-open-link-functions 'org-pass-link-to-system)
;;(add-hook 'org-mode-hook 'wrap-region-mode)
(add-hook 'latex-mode-hook 'wrap-region-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode);           Turn on math-mode by default(setq font-latex-fontify-sectioning 'color)
(add-hook 'after-make-frame-functions 'fit-frame)

;; Org-mode todo keywords and faces

(setq org-todo-keywords
      '(
        (sequence "TODO(t)" "NEXT(n)" "WAITING(w)" "|" "DONE(d)")
        (sequence "|" "CANCELED(c)" "SOMEDAY(f)")
        ))

(setq org-todo-keyword-faces
       '(("NEXT" . (:foreground "IndianRed1" :weight bold))
        ("WAITING" . (:foreground "IndianRed1" :weight bold))
        ("SOMEDAY" . (:foreground "LimeGreen" :weight bold))
        ))

;; faces
(font-lock-add-keywords
 'org-mode
 '(
   ("&\\([^& \n]+\\)&" (0 '(:foreground "green") t)) ;; add text
   (">\\([^>\n]+\\)<" (0 '(:foreground "orange") t)) ;; add comment
   ("==\\([^==\n]+\\)==" (0 '(:weight ultra-bold :foreground "black" :background "yellow") t))) ;; highlight
)

(use-package helm-org)

(load-library "find-lisp")
(add-hook 'org-agenda-mode-hook (lambda ()
(setq org-agenda-files
   (find-lisp-find-files org-files "\.org$"))
))

;; Custom agenda
(setq org-agenda-custom-commands
      '(("W" "Weekly Tasks"
            ((tags "WK"
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "Task to complete this week:")
                 ))
          (agenda "")) )
        ("D" "Daily Tasks"
            ((tags "TDAY"
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "Task to complete this week:")
                 ))
          (agenda "")))

))
(setq org-agenda-sorting-strategy
  (quote
   ((agenda deadline-up priority-down)
    (todo priority-down category-keep)
    (tags priority-down category-keep)
    (search category-keep))))

(org-link-set-parameters
 "calc"
 :export (lambda (path desc backend)
           (cond
            ((eq 'latex backend)
             (format "\\texttt{%s}" desc))))
 :face '(:foreground "chartreuse"))

;; special markup for journals
(org-link-set-parameters
 "response"
 :export (lambda (keyword desc format)
           (cond
            ((eq 'latex format)
             (format "\\textcolor{red}{%s}" keyword))))
 :face '(:foreground "red"))

(setq org-emphasis-alist
      (cons '("+" '(:foreground "red"))
            (delete* "+" org-emphasis-alist :key 'car :test 'equal)))

(custom-set-variables 
  '(org-latex-text-markup-alist '((bold . "\\textbf{%s}")
                                         (code . verb)
                                         (italic . "\\emph{%s}")
                                         (strike-through . "\\textcolor{red}{%s}")
                                         (underline . "\\uline{%s}")
                                         (verbatim . protectedtexttt))))

(setq org-src-blocks '(("response" (:foreground "red"))))
(defface org-block
  ;; defface org-block-background was removed from org: http://emacs.stackexchange.com/questions/14824/org-block-background-font-not-having-effect
  '((t (:background "#1a1a1a")))
  "Face used for the source block background.")

(setq image-file-name-extensions
  (quote
   ("png" "jpeg" "jpg" "gif" "tiff" "tif" "xbm" "xpm" "pbm" "pgm" "ppm" "pnm" "svg" "pdf" "bmp" "eps")))
(add-to-list 'image-type-file-name-regexps '("\\.eps\\'" . imagemagick))
(add-to-list 'image-file-name-extensions "eps")
(add-to-list 'image-type-file-name-regexps '("\\.pdf\\'" . imagemagick))
(add-to-list 'image-file-name-extensions "pdf")
(setq imagemagick-types-inhibit (remove 'PDF imagemagick-types-inhibit))
(setq org-image-actual-width 600)

(setq TeX-parse-self t
      TeX-quote-after-quote nil
      LaTeX-german-quote-after-quote nil
      LaTeX-babel-hyphen nil
      TeX-command-force "XeLaTeX")  
 (add-hook 'LaTeX-mode-hook (lambda()
 (setq TeX-save-query nil)
 ))

(setq weasel-words-regex
      (concat "\\b" (regexp-opt
                     '("one of the"
                       "should"
                       "for the case of"
                       "in the case"
                       "despite the fact"
                       "owing to the fact"
                       "devoid of"
                       "comparable to"
                       "cognizant"
                       "clearly"
                       "in that case"
                       "center around"
                       "by no means"
                       "by virtue of"
                       "as a result of"
                       "as a consequence of"
                       "as a way to"
                       "as a whole"
                       "arguably"
                       "anticipate that"
                       "anterior"
                       "absolutely"
                       "just"
                       "sort of"
                       "a lot"
                       "probably"
                       "maybe"
                       "perhaps"
                       "I think"
                       "really"
                       "pretty"
                       "maybe"
                       "nice"
                       "in many cases"
                       "make a decision"
                       "make an attempt"
                       "conduct an investigation"
                       "perform an experiment"
                       "action"
                       "utilize"
                       "It is interesting to note"
                       "Note that"
                       "leverage") t) "\\b"))

;; out my own jargon
(setq artbollocks nil)

;; LaTeX compilation command
     (setq org-export-latex-emphasis-alist (quote (("*" "\\textbf{%s}" nil)
     ("/" "\\emph{%s}" nil) ("_" "\\alert{%s}" nil) ("+" "\\st{%s}" nil)
     ("=" "\\verb" t) (" <at> " "\\alert{%s}" nil) ("!" "\\note{%s}" nil) ("~"
     "\\verb" t))))

(use-package pirl-mode
 :load-path "lisp/")

;; Journal notebooks
;; http://www.howardism.org/Technical/Emacs/journaling-org.htm
;; Aqeel Akber, 2016 (@AdmiralAkber)

;; Author name to be auto inserted in entries
(setq journal-author "Marvin Doyley")

;; This is the base folder where all your "books"
;; will be stored. 
(setq journal-base-dir org-files)


;; These are your "books" (folders), add as many as you like.
;; Note: "sub volumes" are acheivable with sub folders.
(setq journal-books '("Teaching"
                      "Research"
              "Personal"))

;; Functions for journal
(defun get-journal-file-today (book)
  "Return today's filename for a books journal file."
  (interactive (list (completing-read "Book: " journal-books) ))
  (expand-file-name
   (concat journal-base-dir book "/J"
           (format-time-string "%Y%m%d") ".org" )) )

(defun journal-today ()
  "Load todays journal entry for book"
  (interactive)
  (find-file (call-interactively 'get-journal-file-today)) )

(defun journal-entry-date ()
  "Inserts the journal heading based on the file's name."
  (when (string-match
         "\\(J\\)\\(20[0-9][0-9]\\)\\([0-9][0-9]\\)\\([0-9][0-9]\\)\\(.org\\)"
         (buffer-name))
    (let ((year  (string-to-number (match-string 2 (buffer-name))))
          (month (string-to-number (match-string 3 (buffer-name))))
          (day   (string-to-number (match-string 4 (buffer-name))))
          (datim nil))
      (setq datim (encode-time 0 0 0 day month year))
      (format-time-string "%Y-%m-%d (%A)" datim))))

;; Auto-insert journal header
(auto-insert-mode)
(eval-after-load 'autoinsert
  '(define-auto-insert
     '("\\(J\\)\\(20[0-9][0-9]\\)\\([0-9][0-9]\\)\\([0-9][0-9]\\)\\(.org\\)" . "Journal Header")
     '("Short description: "
       "#+TITLE: Journal Entry - "
       (car
        (last
         (split-string
          (file-name-directory buffer-file-name) "/ORG/"))) \n
       (concat "#+AUTHOR: " journal-author) \n
       "#+DATE: " (journal-entry-date) \n
       "#+FILETAGS: "
       (car
        (last
         (split-string
          (file-name-directory buffer-file-name) "/ORG/"))) \n \n
       > _ \n
       )))

;; Journal Key bindings
(global-set-key (kbd "C-c j") 'journal-today)

;; function for increasing line spacing
(defun double-space ()
  "Make buffer look approximately double-spaced."
  (interactive)
  (setq line-spacing 10))


(defun single-space ()
  "Make buffer single-spaced."
  (interactive)
  (setq line-spacing nil))

(require 'epa-file)
(custom-set-variables '(epg-gpg-program  "/usr/local/MacGPG2/bin/gpg2"))
(epa-file-enable)

;; Org-mode markup commands
;; * Markup commands for org-mode
(loop for (type beginning-marker end-marker)
      in '((subscript "_{" "}")
           (superscript "^{" "}")
           (italics "/" "/")
           (bold "*" "*")
           (verbatim "=" "=")
           (code "~" "~")
           (underline "_" "_")
           (strikethrough "+" "+"))
      do
      (eval `(defun ,(intern (format "org-%s-region-or-point" type)) ()
               ,(format "%s the region, word or character at point"
                        (upcase (symbol-name type)))
               (interactive)
               (cond
                ((region-active-p)
                 (goto-char (region-end))
                 (insert ,end-marker)
                 (goto-char (region-beginning))
                 (insert ,beginning-marker)
                 (re-search-forward (regexp-quote ,end-marker))
                 (goto-char (match-end 0)))
                ((thing-at-point 'word)
                 (cond
                  ((looking-back " " 1)
                   (insert ,beginning-marker)
                   (re-search-forward "\\>")
                   (insert ,end-marker))
                  (t
                   (re-search-backward "\\<")
                   (insert ,beginning-marker)
                   (re-search-forward "\\>")
                   (insert ,end-marker))))

                (t
                 (insert ,(concat beginning-marker end-marker))
                 (backward-char ,(length end-marker)))))))

(defun org-latex-math-region-or-point (&optional arg)
  "Wrap the selected region in latex math markup.
\(\) or $$ (with prefix ARG) or @@latex:@@ with double prefix.
Or insert those and put point in the middle to add an equation."
  (interactive "P")
  (let ((chars
         (cond
          ((null arg)
           '("\\(" . "\\)"))
          ((equal arg '(4))
           '("$" . "$"))
          ((equal arg '(16))
           '("@@latex:" . "@@")))))
    (if (region-active-p)
        (progn
          (goto-char (region-end))
          (insert (cdr chars))
          (goto-char (region-beginning))
          (insert (car chars)))
      (insert (concat  (car chars) (cdr chars)))
      (backward-char (length (cdr chars))))))


(defun helm-insert-org-entity ()
  "Helm interface to insert an entity from `org-entities'.
F1 inserts utf-8 character
F2 inserts entity code
F3 inserts LaTeX code (does not wrap in math-mode)
F4 inserts HTML code
F5 inserts the entity code."
  (interactive)
  (helm :sources
        (reverse
         (let ((sources '())
               toplevel
               secondlevel)
           (dolist (element (append
                             '("* User" "** User entities")
                             org-entities-user org-entities))
             (when (and (stringp element)
                        (s-starts-with? "* " element))
               (setq toplevel element))
             (when (and (stringp element)
                        (s-starts-with? "** " element))
               (setq secondlevel element)
               (add-to-list
                'sources
                `((name . ,(concat
                            toplevel
                            (replace-regexp-in-string
                             "\\*\\*" " - " secondlevel)))
                  (candidates . nil)
                  (action . (("insert utf-8 char" . (lambda (x)
                                                      (mapc (lambda (candidate)
                                                              (insert (nth 6 candidate)))
                                                            (helm-marked-candidates))))
                             ("insert org entity" . (lambda (x)
                                                      (mapc (lambda (candidate)
                                                              (insert
                                                               (concat "\\" (car candidate))))
                                                            (helm-marked-candidates))))
                             ("insert latex" . (lambda (x)
                                                 (mapc (lambda (candidate)
                                                         (insert (nth 1 candidate)))
                                                       (helm-marked-candidates))))
                             ("insert html" . (lambda (x)
                                                (mapc (lambda (candidate)
                                                        (insert (nth 3 candidate)))
                                                      (helm-marked-candidates))))
                             ("insert code" . (lambda (x)
                                                (mapc (lambda (candidate)
                                                        (insert (format "%S" candidate)))
                                                      (helm-marked-candidates)))))))))
             (when (and element (listp element))
               (setf (cdr (assoc 'candidates (car sources)))
                     (append
                      (cdr (assoc 'candidates (car sources)))
                      (list (cons
                             (format "%10s %s" (nth 6 element) element)
                             element))))))
           sources))))


(defun ivy-insert-org-entity ()
  "Insert an org-entity using ivy."
  (interactive)
  (ivy-read "Entity: " (loop for element in (append org-entities org-entities-user)
                             when (not (stringp element))
                             collect
                             (cons 
                              (format "%10s | %s | %s | %s"
                                      (car element) ;name
                                      (nth 1 element) ; latex
                                      (nth 3 element) ; html
                                      (nth 6 element)) ;utf-8
                              element))
            :require-match t
            :action '(1
                      ("u" (lambda (element) (insert (nth 6 (cdr element)))) "utf-8")
                      ("o" (lambda (element) (insert "\\" (cadr element))) "org-entity")
                      ("l" (lambda (element) (insert (nth 1 (cdr element)))) "latex")
                      ("h" (lambda (element) (insert (nth 3 (cdr element)))) "html"))))

(defun kill-matching-lines (regexp &optional rstart rend interactive)
  "Kill lines containing matches for REGEXP.

See `flush-lines' or `keep-lines' for behavior of this command.

If the buffer is read-only, Emacs will beep and refrain from deleting
the line, but put the line in the kill ring anyway.  This means that
you can use this command to copy text from a read-only buffer.
\(If the variable `kill-read-only-ok' is non-nil, then this won't
even beep.)"
  (interactive
   (keep-lines-read-args "Kill lines containing match for regexp"))
  (let ((buffer-file-name nil)) ;; HACK for `clone-buffer'
    (with-current-buffer (clone-buffer nil nil)
      (let ((inhibit-read-only t))
        (keep-lines regexp rstart rend interactive)
        (kill-region (or rstart (line-beginning-position))
                     (or rend (point-max))))
      (kill-buffer)))
  (unless (and buffer-read-only kill-read-only-ok)
    ;; Delete lines or make the "Buffer is read-only" error.
    (flush-lines regexp rstart rend interactive)))

;; Emacs key bindings 
(global-set-key (kbd "M-s e")  'mu4e)   ;; Start mu4e email client
;; redefine keyboard shortcuts
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\M-SPC" 'set-mark-command)
(global-set-key "\M-w" 'kill-ring-save)
(global-set-key (kbd "\C-xs") 'ispell-word)
(global-set-key "\M-c" 'capitalize-word)
(global-set-key "\M-l" 'downcase-word)
(global-set-key "\M-u" 'upcase-word)
(global-set-key "\C-cv" 'replace-word-under-cursor)
(global-set-key "\C-n" 'org-show-next-slide)
(global-set-key "\C-p" 'org-show-previous-slide)
(global-set-key (kbd "s-z") 'undo)
(global-set-key (kbd "TAB") 'tab-to-tab-stop)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "<C-S-up>")     'buf-move-up)
(global-set-key (kbd "<C-S-down>")   'buf-move-down)
(global-set-key (kbd "<C-S-left>")   'buf-move-left)
(global-set-key (kbd "<C-S-right>")  'buf-move-right)
(global-set-key (kbd "C-}")  'org-ref-helm-insert-cite-link)
(global-set-key (kbd "M-s s")   #'helm-ag)
(global-set-key (kbd "M-s t") 'words-speak)
(global-set-key [f10] 'org-ref-open-bibtex-notes)
(global-set-key [f11] 'org-ref-open-bibtex-pdf)
(global-set-key [f12] 'org-ref-open-in-browser)
(global-set-key (kbd "C-x c!")   #'helm-calcul-expression)
(global-set-key (kbd "C-x c:")   #'helm-eval-expression-with-eldoc)
(global-set-key [M-down-mouse-1] 'mouse-drag-secondary-pasting)
(global-set-key [M-S-down-mouse-1] 'mouse-drag-secondary-moving)
(define-key helm-map (kbd "M-o") #'helm-previous-source)
(define-key global-map "\C-cc" 'helm-org-capture-templates);
(define-key 'help-command (kbd "C-f") 'helm-apropos)
(define-key 'help-command (kbd "r") 'helm-info-emacs)
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)

(use-package pirl-mode
  :ensure nil
  :load-path "lisp/"
  :init (require 'pirl-mode)
  :config (pirl-mode))
