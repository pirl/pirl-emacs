;; Setup
(setq TeX-save-query nil)
(setq TeX-electric-sub-and-superscript t)
(setq TeX-PDF-mode t)
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq global-font-lock-mode t)
(setq LaTeX-paragraph-commands '("newpage" "usepackage"))
(setq TeX-newline-function 'reindent-then-newline-and-indent)

;;====================================
;; 			Hooks
;;======================================

(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode) ;turn on math-mode by default   
(add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)   
(add-hook 'after-make-frame-functions 'fit-frame)
(autoload 'hide-mode-line "hide-mode-line" nil t)
(setq pd-folding-done-p nil)
(make-variable-buffer-local 'pd-folding-done-p)
(put 'pd-folding-done-p 'permanent-local t)
(add-hook 'LaTeX-mode-hook
          (lambda ()
            (TeX-fold-mode 1)))
(setq preview-scale-function 2)
(setq TeX-outline-extra
      '(("[ \t]*\\\\\\(bib\\)?item\\b" 7)
        ("\\\\bibliography\\b" 2)))

;; Only change sectioning color
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode);           Turn on math-mode by default(setq font-latex-fontify-sectioning 'color)
(add-hook 'LaTeX-mode-hook 'turn-on-real-auto-save)
(setq ispell-parser 'tex)


;; Latex Path

(setq TeX-save-query nil)
(setq TeX-show-compilation t)

(setq TeX-parse-self t
      TeX-quote-after-quote nil
      LaTeX-german-quote-after-quote nil
      LaTeX-babel-hyphen nil)

;; Setup
(setq TeX-save-query nil)
(setq TeX-electric-sub-and-superscript t)
(setq TeX-engine 'pdflatex)
(setq TeX-PDF-mode t)
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq global-font-lock-mode t)
(setq LaTeX-paragraph-commands '("newpage" "usepackage"))
(setq TeX-newline-function 'reindent-then-newline-and-indent)

;
;; auto fill mode for text-mode

 (setq text-mode-hook '(lambda()
		(auto-fill-mode 1)
		(setq fill-column 110)))

(setq-default TeX-master nil)
;; reftex

;;==============================================
;;                RefTeX
;;===============================================
(autoload 'reftex-mode "reftex" "RefTeX Minor Mode" t)
(autoload 'turn-on-reftex "reftex" "RefTeX Minor Mode" nil)
(autoload 'reftex-citation "reftex-cite" "Make citation" nil)
(autoload 'reftex-index-phrase-mode "reftex-index" "Phrase mode" t)
(add-hook 'latex-mode-hook 'turn-on-reftex) ; with Emacs latex mode
(add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'org-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)

(provide 'pirl-latex)
