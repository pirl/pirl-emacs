


(require 'easymenu)

(defvar pirl-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "\e\er") 'email-region)
    (define-key map (kbd "\e\eh") 'email-heading)
    (define-key map (kbd "\e\ep") 'ox-manuscript-export-and-build-and-email)
    (define-key map (kbd "\e\ea") 'ox-archive-create-and-mail)
    ;; insert keys
    (define-key map (kbd "\e\eR") 'org-ref-insert-ref-link)
    (define-key map (kbd "\e\eC") 'org-ref-insert-cite-link)
    (define-key map (kbd "\e\ef") 'org-ref-notes-at-point)
    map))

;
(easy-menu-define my-menu pirl-mode-map "My own menu"
  '("Research Tools"
    ("org-mode"
     ["Toggle symbols" org-toggle-pretty-entities t]
     ["Toggle inline images" org-toggle-inline-images t]
     ["Toggle LaTeX images" org-toggle-latex-fragment t])
    ("Miscel"
     ["Writing guide" pirl:wguide t]
     ["Cheat Sheet" pirl:help t]
     ["Open notes at point" org-ref-open-notes-at-point t]
     ["Open PDF at point" org-ref-open-pdf-at-point t]
     )
    ("Export"
     ["Typeset Latex" ox-manuscript-latex-pdf-process t]   
     ["Make PDF" ox-manuscript-export-and-build-and-open t]   
     ["Build and Email PDF" ox-manuscript-export-and-build-and-email t]
     ["Prepare manuscript for submission" ox-manuscript-build-submission-manuscript-and-open t]
     ["Clean up" ox-manuscript-cleanup t]   
     ["Export to pdf" org-export-dispatch]
     )
    ("Markup"
     ["Bold" org-bold-region-or-point t]
     ["Italics" org-italics-region-or-point]
     ["Underline" org-underline-region-or-point t]
     ["Strikethrough" org-strikethrough-region-or-point t]
     ["Verbatim" org-verbatim-region-or-point t]
     ["Code" org-code-region-or-point t]
     ["Superscript" org-superscript-region-or-point t]
     ["Subscript" org-subscript-region-or-point t]
     ["Latex snippet" org-latex-math-region-or-point t])
    ("words"
     ["Menu" words t]
     ["Web of Knowledge" wos t]
     ["Pubmed" pubmed t]
     ["Crossref" crossref t]
     ["Lookup in Google" words-google t]
     ["Lookup in WOS" words-wos t]
     ["Lookup in Pubmed" words-pubmed t]
     ["Lookup in Crossref" words-crossref t]
     ["Lookup in Arxiv" words-arxiv]
     ["Lookup in Dictionary" words-dictionary t]
     ["Lookup in Thesaurus" words-thesaurus t])
    ("Babel"
     ["Excecute all code blocks in buffer" org-babel-execute-buffer t]
     ["Extract code blocks to filer" org-babel-tange t])
    ("Book-Marks"
     ["List all bookmarks" list-bookmarks t]
     ["Set bookmark" bookmark-set t]
     ["Clean upbookmarks" bookmark-delete t])
    ("Email"
     ["Read Email" mu4e t]
     ["Send Email" mail t]
     )
    ("Track changes"
     ["Accept/reject changes" cm-accept/reject-all-changes t]
     ["Track change hydra" cm/body t])
    ["--" Favorities]
    ["Lookup" google-this t]
    ["Textlint" textlint-run t]    
    ["Pubmed Search" pubmed-simple-search t]
    ["Todo.org" (find-file "~/Org-files/General/Master-todo.org")]
    ["PIRL_todo" (find-file "~/Org-files/Planner.org")]
    ["Projects" (dired "~/Org-files")]
    ["ECE 447" (dired "/Users/doyley/Dropbox/Filing_Cabinet/T/Teaching2/ECE 447")]
    ["Read Email" mu4e t]
    ["Full Screen" toggle-frame-maximized t]
    ["Go to directory" dired t]
    ))


(define-minor-mode pirl-mode
  "Minor mode for PIRL

\\{PIRL-mode-map}"
  :lighter " PIRL"
  :global t
  :keymap pirl-mode-map)

(provide 'pirl-mode)
