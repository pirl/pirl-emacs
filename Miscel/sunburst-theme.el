;;   "Color theme by dngpng, created 2007-09-11."
;; Originally, designed for color-theme.el, this is a port to
;; emacs deftheme framework.  Although the colors are the same,
;; it has been heavily reorganized
(deftheme sunburst "The Sunburst Color Theme")

(custom-theme-set-faces
 'sunburst

 ;; emacs
 '(cursor ((t (:background "yellow"))))
 '(default ((t (:background "#111" :foreground "#ddd"))))
 '(mode-line ((t (:background "#e6e5e4" :foreground "black"))))
 '(highlight ((t (:bold t :slant italic))))
 '(gui-element ((t (:background "#0e2231" :foreground "black"))))
 '(blue ((t (:foreground "blue"))))
 '(italic ((t (nil))))
 '(left-margin ((t (nil))))
 '(bold ((t (:bold t))))
 '(bold-italic ((t (:bold t :slant italic))))
 '(border-glyph ((t (nil))))
 '(underline ((nil (:underline nil))))
 '(primary-selection ((t (:background "#222"))))
 '(border-coler ((t (:background "#111"))))
 '(region ((t (:background "#4a410d"))))

 ;; font-lock
 '(font-lock-builtin-face ((t (:foreground "#dd7b3b"))))
 '(font-lock-builtin-face ((t (:foreground "#dd7b3b"))))
 '(font-lock-comment-face ((t (:foreground "#666" ))))
 '(font-lock-constant-face ((t (:foreground "#99cf50"))))
 '(font-lock-doc-string-face ((t (:foreground "#9b859d"))))
 '(font-lock-function-name-face ((t (:foreground "#e9c062" :bold t))))
 '(font-lock-keyword-face ((t (:foreground "#cf6a4c" :bold t))))
 '(font-lock-preprocessor-face ((t (:foreground "#aeaeae"))))
 '(font-lock-reference-face ((t (:foreground "#8b98ab"))))
 '(font-lock-string-face ((t (:foreground "#65b042"))))
 '(font-lock-type-face ((t (:foreground "#c5af75"))))
 '(font-lock-variable-name-face ((t (:foreground "#3387cc"))))
 '(font-lock-warning-face ((t (:bold t :background "#420e09" :foreground "#eeeeee"))))

 ;; erc
 '(erc-current-nick-face ((t (:foreground "#aeaeae"))))
 '(erc-default-face ((t (:foreground "#ddd"))))
 '(erc-keyword-face ((t (:foreground "#cf6a4c"))))
 '(erc-notice-face ((t (:foreground "#666"))))
 '(erc-timestamp-face ((t (:foreground "#65b042"))))
 '(erc-underline-face ((t (:foreground "#c5af75"))))

 ;; nxml
 '(nxml-attribute-local-name-face ((t (:foreground "#3387cc"))))
 '(nxml-attribute-colon-face ((t (:foreground "#e28964"))))
 '(nxml-attribute-prefix-face ((t (:foreground "#cf6a4c"))))
 '(nxml-attribute-value-face ((t (:foreground "#65b042"))))
 '(nxml-attribute-value-delimiter-face ((t (:foreground "#99cf50"))))
 '(nxml-namespace-attribute-prefix-face ((t (:foreground "#9b859d"))))
 '(nxml-comment-content-face ((t (:foreground "#666"))))
 '(nxml-comment-delimiter-face ((t (:foreground "#333"))))
 '(nxml-element-local-name-face ((t (:foreground "#e9c062"))))
 '(nxml-markup-declaration-delimiter-face ((t (:foreground "#aeaeae"))))
 '(nxml-namespace-attribute-xmlns-face ((t (:foreground "#8b98ab"))))
 '(nxml-prolog-keyword-face ((t (:foreground "#c5af75"))))
 '(nxml-prolog-literal-content-face ((t (:foreground "#dad085"))))
 '(nxml-tag-delimiter-face ((t (:foreground "#cda869"))))
 '(nxml-tag-slash-face ((t (:foreground "#cda869"))))
 '(nxml-text-face ((t (:foreground "#ddd"))))

 ;; jabber
 '(jabber-chat-prompt-local ((t (:foreground "#65b042"))))
 '(jabber-chat-prompt-foreign ((t(:foreground "#3387cc"))))
 '(jabber-roster-user-xa ((t (:foreground "#e28964"))))
 '(jabber-roster-user-online ((t (:foreground "#3387cc"))))
 '(jabber-roster-user-away ((t (:foreground "#9b859d"))))

 ;; magit
 '(magit-log-sha1 ((t (:foreground "#cf6a4c"))))
 '(magit-log-head-label-local ((t (:foreground "#3387cc"))))
 '(magit-log-head-label-remote ((t (:foreground "#65b042"))))
 '(magit-branch ((t (:foreground "#e9c062"))))
 '(magit-section-title ((t (:foreground "#9b859d"))))
 '(magit-item-highlight ((t (:background "#1f1f1f"))))
 
 ;; Outline Mode and Org-Mode
 
        `(outline-1 ((t (:foreground ,"LightSkyBlue" :bold t))))
        `(outline-2 ((t (:foreground ,"LightGoldenrod" :slant italic))))
        `(outline-3 ((t (:foreground ,"Cyan1" :bold t))))
        `(outline-4 ((t (:foreground ,"chocolate1" :bold t))))
        `(outline-5 ((t (:foreground ,"LightSteelBlue" :bold nil))))
        `(org-level-1 ((t (:inherit outline-1))))
        `(org-level-2 ((t (:inherit outline-2))))
        `(org-level-3 ((t (:inherit outline-3))))
        `(org-level-4 ((t (:inherit outline-4))))
        `(org-level-5 ((t (:inherit outline-5))))
        `(org-level-6 ((t (:foreground ,"PaleGreen"))))
        `(org-level-7 ((t (:foreground ,"#88CCEE"))))
        `(org-level-8 ((t (:foreground ,"#EE88FF"))))
        `(org-special-keyword ((t (:foreground ,"#FFFF88"))))
        `(org-agenda-date ((t (:inherit font-lock-type-face))))
        `(org-agenda-date-weekend ((t (:inherit org-agenda-date))))
        `(org-scheduled-today ((t (:foreground "#ff6ab9" :italic t))))
        `(org-scheduled-previously ((t (:foreground "#d74b4b"))))
        `(org-upcoming-deadline ((t (:foreground "#d6ff9c"))))
        `(org-warning ((t (:foreground "#d74b4b" :italic t :bold t))))
        `(org-table ((t (:foreground ,"#99DDFF"))))
        `(org-formula ((t (:foreground ,"#FF7777"))))
        `(org-date ((t (:foreground ,"#55DDCC"))))
        `(org-link ((t (:foreground "Turquoise" :weight bold ))))
        `(org-tag ((t (:foreground ,"#AAAAAA"))))
        ;;`(org-hide ((t (:foreground "#191919"))))
        ;;`(org-todo ((t (:background ,"#BB0000" :foreground "white" ,@box))))
        ;;`(org-done ((t (:background ,"#004400" :foreground "white" ,@box))))
        ;;`(org-checkbox ((t (:background ,"#DD3333" :foreground "white" ,@box))))
        ;;`(org-checkbox-statistics-todo ((t (:background ,"#DD3333" :foreground "white" ,@box))))
        ;;`(org-checkbox-statistics-done ((t (:background ,"#DD3333" :foreground "white" ,@box))))
        `(org-column ((t (:background ,"#222222"))))
        ;;`(org-column-title ((t (:background ,"DarkGreen" :foreground "white" ,@box))))
        ;;`(cd-org-todo-kwd-face ((t (:background ,"#BB0000" :foreground "white" ,@box))))
        ;;`(cd-org-done-kwd-face ((t (:background ,"#004400" :foreground "white" ,@box))))
        ;;`(cd-org-project-kwd-face ((t (:background ,"#774488" :foreground "white" ,@box))))
        ;;`(cd-org-waiting-kwd-face ((t (:background ,"#EE9900" :foreground "white" ,@box))))
        ;;`(cd-org-someday-kwd-face ((t (:background ,"#444444" :foreground "white" ,@box))))
        ;;`(cd-org-started-kwd-face ((t (:background ,"#EE6600" :foreground "white" `@box))))
        ;;`(cd-org-cancelled-kwd-face ((t (:background ,"#777700" :foreground "white" ,@box))))
        ;;`(cd-org-delegated-kwd-face ((t (:background ,"#BB9900" :foreground "white" ,@box))))
		
 ;; highline
 '(highline-face ((t (:background "#4a410d"))))

 ;; MultiMode
 '(mmm-default-submode-face ((t (:background "#111"))))
 )

(provide-theme 'sunburst)
